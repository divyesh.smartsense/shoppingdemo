import { Component, OnInit } from '@angular/core';
import {AuthService} from "../auth.service";
import {Router} from "@angular/router";
import {of} from "rxjs";
import {Input} from "@angular/core";
import {ShoppingCartService} from "../shopping-cart.service";
import {ProductService} from "../product.service";
import {Product} from "../models/product";

@Component({
  selector: 'bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.css']
})
export class BsNavbarComponent implements OnInit {
    totalQuantity:number;
    @Input('product') product: Product;

  constructor(private afAuth: AuthService ,
              private route: Router ,
              private shoppingCartService: ShoppingCartService,
              private productService: ProductService)  {}

    logout(){
    this.afAuth.logout();
    localStorage.clear();
    this.afAuth.user$=of(null);
    this.route.navigate(['/login']);
  }
    ngOnInit() {
        const cartId = localStorage.getItem('cartId');
        this.productService.getAllOfProductId(cartId).valueChanges()
            .subscribe(response => {
                this.totalQuantity = 0;
                for (let productData in response) {
                    this.totalQuantity += response[productData].quantity;
                }
            });
    }

}
