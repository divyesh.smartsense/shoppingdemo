import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {TabletsService} from "../tablets.service";
import {Tablets} from "../tablets";

@Component({
  selector: 'app-tablet-detail',
  templateUrl: './tablet-detail.component.html',
  styleUrls: ['./tablet-detail.component.css']
})
export class TabletDetailComponent implements OnInit {

  tablet: Tablets;
  constructor(private route: ActivatedRoute , private tabletsService: TabletsService) { }

  ngOnInit() {
    this.getTabletDetail();
  }
  getTabletDetail(){
    const id = +this.route.snapshot.paramMap.get('id');
    this.tabletsService.setTabletDetail(id)
        .subscribe( tablet => this.tablet = tablet);
  }

}
