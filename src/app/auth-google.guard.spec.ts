import { TestBed, async, inject } from '@angular/core/testing';

import { AuthGoogleGuard } from './auth-google.guard';

describe('AuthGoogleGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGoogleGuard]
    });
  });

  it('should ...', inject([AuthGoogleGuard], (guard: AuthGoogleGuard) => {
    expect(guard).toBeTruthy();
  }));
});
