export class Tablets {
    id: number;
    url: string;
    name: string;
    price: number;
    performance: string;
    display: number;
    camera: string;
    battery: number;
}
