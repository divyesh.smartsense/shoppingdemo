import { TestBed } from '@angular/core/testing';

import { TabletsService } from './tablets.service';

describe('TabletsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TabletsService = TestBed.get(TabletsService);
    expect(service).toBeTruthy();
  });
});
