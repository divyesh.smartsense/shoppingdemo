import { Component, OnInit } from '@angular/core';
import {Laptops} from "./laptops";
import {LaptopsService} from "./laptops.service";

@Component({
  selector: 'app-laptops',
  templateUrl: './laptops.component.html',
  styleUrls: ['./laptops.component.css']
})
export class LaptopsComponent implements OnInit {
    laptops: Laptops[] = [];
  constructor(private laptopsService: LaptopsService) { }

  ngOnInit() {
    this.getLaptops();
  }
  getLaptops(){
    this.laptopsService.setLaptops()
        .subscribe(laptops => this.laptops = laptops);
  }

}
