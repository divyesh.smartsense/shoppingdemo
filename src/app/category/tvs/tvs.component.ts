import { Component, OnInit } from '@angular/core';
import {Tvs} from "./tvs";
import {TvsService} from "./tvs.service";

@Component({
  selector: 'app-tvs',
  templateUrl: './tvs.component.html',
  styleUrls: ['./tvs.component.css']
})
export class TvsComponent implements OnInit {
      tvs: Tvs[] = [];
  constructor(private tvsService: TvsService) { }

  ngOnInit() {
    this.getTvs();
  }
  getTvs(){
    this.tvsService.setTvs()
        .subscribe(tvs => this.tvs =tvs);
  }

}
