import {Component, OnInit} from '@angular/core';
import {ProductService} from "../product.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Product} from "../models/product";

@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.css']
})
export class ProductsComponent {
    products: any[] = [];
    filteredProducts: Product[] = [];
    category: string;

    constructor(private productService: ProductService,
                private router: Router,
                private route: ActivatedRoute) {

        productService.getAllProduct().valueChanges().subscribe(
            products => {
                this.products = products;
                route.queryParamMap.subscribe(
                    params => {
                        this.category = params.get('category');
                        this.filteredProducts = (this.category) ?
                            this.products.filter(p => p.category.toLowerCase() === this.category.toLowerCase()) :
                            this.products
                    });
            }
        );
    }
}
