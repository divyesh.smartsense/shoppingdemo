import {Component, Input, OnInit} from '@angular/core';
import {ProductService} from "../product.service";
import {Product} from "../models/product";
import {ActivatedRoute} from "@angular/router";
import {ShoppingCart} from "../models/shopping-cart";
import {ShoppingCartService} from "../shopping-cart.service";
import {Subscription} from "rxjs";
import {ShoppingCartItem} from "../models/shopping-cart-item";
@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

    constructor(private shoppigCartService: ShoppingCartService, private productService: ProductService, private route: ActivatedRoute) {
    }

    totalQuantity: number;
    @Input('product') product: Product;
    @Input('show-actions') showActions = true;
    @Input('shopping-cart') shoppingCart: ShoppingCart;
    productTitle: ShoppingCartItem[];
    productArray = [];
    sum : number;


    ngOnInit() {
        const cartId = localStorage.getItem('cartId');
        this.productService.getAllOfProductId(cartId).valueChanges()
            .subscribe(response => {
                this.totalQuantity = 0;
                this.sum = 0;
                let prodTitle: string;
                for (let productData in response) {
                    this.totalQuantity += response[productData].quantity;
                    this.productTitle = response[productData].product;
                    //console.log(this.productTitle);
                    this.productArray.push(response[productData]);
                    this.sum += (response[productData].product.price *response[productData].quantity);

                }
            });
    }

    clearCartData(){
        this.shoppigCartService.clearCart();
    }
}
