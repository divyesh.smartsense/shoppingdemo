import {Tvs} from "./tvs";
export const TVS: Tvs[] =[
    {
        id:1,
        url: "https://cdn.vsbytes.com/wp-content/uploads/2017/12/KLV-32W672F.jpg",
        name: "Sony Bravia KLV",
        price: 42900,
        size: 32
    },
    {
        id:2,
        url: "https://cdn.vsbytes.com/wp-content/uploads/2017/12/vu-40d6575.jpeg",
        name: "Vu 40D6575",
        price: 48999,
        size: 50
    },
    {
        id:3,
        url: "https://cdn.vsbytes.com/wp-content/uploads/2018/08/Mi-32-inch-4c-pRO-ANDROID-TV.jpg",
        name: "Mi LED TV 4C Pro",
        price: 45500,
        size: 42
    },
    {
        id:4,
        url: "https://cdn.vsbytes.com/wp-content/uploads/2017/12/Sanyo-43-inch-XT.jpg",
        name: "Sanyo XT-43S8100FS",
        price: 39900,
        size: 43
    }
]
