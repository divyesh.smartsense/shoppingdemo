import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class AuthGoogleGuard implements CanActivate {
constructor(private route: Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot){

  if(localStorage.getItem('user-name') !== null){
    return true;
    }
    else {
      this.route.navigate(['/login'] , {queryParams: {returnUrl: state.url}});
    }
  }
}
