export class Laptops {
    id: number;
    url: string;
    name: string;
    price: number;
    performance: string;
    display: number;
    memory: number;
}
