import {Component, Input} from '@angular/core';
import {Product} from "../models/product";
import {ShoppingCartService} from "../shopping-cart.service";
import {AngularFireDatabase} from "@angular/fire/database";
import {ProductService} from "../product.service";
import {ShoppingCart} from "../models/shopping-cart";

@Component({
    selector: 'product-card',
    templateUrl: './product-card.component.html',
    styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent {
    @Input('product') product: Product;
    @Input('show-actions') showActions = true;
    @Input('shopping-cart') shoppingCart: ShoppingCart;
    cartQuantity;
    constructor(private shoppingCartService: ShoppingCartService,
                private productService: ProductService,
                private db: AngularFireDatabase) {
    }

    ngOnInit() {
        const productTitle = this.product.title;
        const cartId = localStorage.getItem('cartId');
        this.productService.getShoppingCardForParticularProduct(cartId, productTitle).valueChanges()
            .subscribe(response => {
                if (response) {
                    this.product.quantity = response['quantity'];
                    this.cartQuantity = response['quantity'];
                }
            });
    }

    addToCart() {
        this.shoppingCartService.addToCartProduct(this.product);
    }
    removeToCart(){
        this.shoppingCartService.removeToCartProduct(this.product);
    }
}


