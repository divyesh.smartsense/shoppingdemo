import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {ProductsComponent} from "./products/products.component";
import {ShoppingCartComponent} from "./shopping-cart/shopping-cart.component";
import {CheckOutComponent} from "./check-out/check-out.component";
import {OrderSuccessComponent} from "./order-success/order-success.component";
import {LoginComponent} from "./login/login.component";
import {AdminProductsComponent} from "./admin/admin-products/admin-products.component";
import {AdminOdersComponent} from "./admin/admin-oders/admin-oders.component";
import {MyOrdersComponent} from "./my-orders/my-orders.component";
import {AuthGoogleGuard} from "./auth-google.guard";
import {ProductFormComponent} from "./admin/product-form/product-form.component";

const appRoutes: Routes = [
    {path: '', component: ProductsComponent},
    {path: 'products' ,component: ProductsComponent},
    {path: 'shopping-cart' , component: ShoppingCartComponent},
    {path: 'login' , component: LoginComponent},

    {path: 'check-out', component: CheckOutComponent , canActivate:[AuthGoogleGuard]},
    {path: 'order-success' , component: OrderSuccessComponent , canActivate:[AuthGoogleGuard]},

    {path:'my/orders' , component: MyOrdersComponent, canActivate:[AuthGoogleGuard]},

    {path: 'admin/products/new' , component: ProductFormComponent , canActivate:[AuthGoogleGuard]},
    {path: 'admin/products/:id' , component: ProductFormComponent , canActivate:[AuthGoogleGuard]},
    {path: 'admin/products' , component: AdminProductsComponent , canActivate:[AuthGoogleGuard]},

    {path: 'admin/orders' , component: AdminOdersComponent , canActivate:[AuthGoogleGuard]}
];

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule.forRoot(appRoutes, {enableTracing: true})
    ],
    exports: [
        RouterModule
    ]

})
export class AppRoutingModule {
}
