import {Mobiles} from "./mobiles";

export const MOBILES: Mobiles[] = [
    {
        id: 1,
        url: "https://www.91-img.com/pictures/129875-v4-apple-iphone-xs-max-mobile-phone-medium-1.jpg",
        name: "Apple iPhone XS Max",
        price: 104900,
        performance: "Hexacore",
        display: 6.5,
        camera: "12MP + 12Mp",
        battery: 3174
    },
    {
        id: 2,
        url: "https://www.91-img.com/pictures/119660-v10-samsung-galaxy-note-9-mobile-phone-medium-1.jpg",
        name: "Samsung Galaxy Note 9",
        price: 59000,
        performance: "Octacore",
        display: 6.4,
        camera: "12MP + 12MP",
        battery: 4000
    },
    {   id: 3,
        url: "https://www.91-img.com/pictures/123814-v5-google-pixel-3-xl-mobile-phone-medium-1.jpg",
        name: "Google Pixel 3 XL",
        price: 70448,
        performance: "Octacore",
        display: 6.3,
        camera: "12.2MP",
        battery: 3430
    },
    {
        id: 4,
        url: "https://www.91-img.com/pictures/132700-v3-honor-view-20-256gb-mobile-phone-medium-1.jpg",
        name: "Honor 20 256GB",
        price: 45999,
        performance: "Octacore",
        display: 6.4,
        camera: "48MP",
        battery: 4000
    },
    {
        id: 5,
        url: "https://www.91-img.com/pictures/127444-v13-oneplus-6t-mobile-phone-medium-1.jpg",
        name: "ONE PLUS 6T",
        price: 37999,
        performance: "Octacore",
        display: 6.41,
        camera: "16MP + 20MP",
        battery: 3700
    },
    {
        id: 6,
        url: "https://www.91-img.com/pictures/128652-v8-nokia-6.1-plus-mobile-phone-medium-1.jpg",
        name: "Nokia 6.1 PLUS",
        price: 14999,
        performance: "Octacore",
        display: 5.81,
        camera: "16MP + 2MP",
        battery: 3060
    },
];
