export class Cameras{
    id: number;
    url: string;
    name: string;
    price: number;
    resolution: number;
    screen: number;
}
