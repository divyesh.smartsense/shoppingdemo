import {Injectable} from '@angular/core';
import {AngularFireDatabase} from "@angular/fire/database";

@Injectable({
    providedIn: 'root'
})
export class ProductService {

    constructor(private db: AngularFireDatabase) {
    }

    createProduct(product) {
        return this.db.list('/products/').push(product);
    }

    getAllProduct() {
        return this.db.list('/products/');
    }

    getShoppingCardForParticularProduct(cartId, productTitle) {
        return this.db.object('/shopping-cart/' + cartId + '/items/' + productTitle);
    }

    getAllOfProductId(cartId) {
        return this.db.object('/shopping-cart/' + cartId + '/items/');
    }

    getProductId(productId) {
        return this.db.object('/products/' + productId);
    }

    updateProduct(product, productId) {
        return this.db.object('/products/' + productId).update(product);
    }

    deleteProduct(productId) {
        return this.db.object('/products/' + productId).remove();
    }
}
