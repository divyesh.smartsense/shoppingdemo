import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  logout(){
    window.localStorage.clear();
    this.route.navigate(['/Login']);
  }
  constructor(private route: Router) { }

  ngOnInit() {
  }

}
