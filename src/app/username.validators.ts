import {AbstractControl, ValidationErrors} from "@angular/forms";

export class UsernameValidators {

   static cannotContainSpace(Control: AbstractControl): ValidationErrors | null {
    if ((Control.value as string).indexOf(' ') >= 0)
      return {cannotContainSpace: true };
    return null;
  }

}
