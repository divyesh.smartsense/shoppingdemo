import { Injectable } from '@angular/core';
import {TVS} from "./mock-tvs";
import {Observable, of} from "rxjs";
import {Tvs} from "./tvs";

@Injectable({
  providedIn: 'root'
})
export class TvsService {
  constructor() { }

  setTvs(): Observable<Tvs[]>{
        return of(TVS);
  }
  setTvsDetail(id: number): Observable<Tvs>{
          return of(TVS.find(tvs => tvs.id === id));
  }
}
