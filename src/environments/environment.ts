// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase:{
    apiKey: "AIzaSyA3JXump5pjvKy1L4TdWNd-AKG-krjYv1Q",
    authDomain: "shoppingdemo-7b36d.firebaseapp.com",
    databaseURL: "https://shoppingdemo-7b36d.firebaseio.com",
    projectId: "shoppingdemo-7b36d",
    storageBucket: "",
    messagingSenderId: "972182501755"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
