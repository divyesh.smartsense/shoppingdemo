import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {LaptopsService} from "../laptops.service";
import {Laptops} from "../laptops";

@Component({
  selector: 'app-laptop-detail',
  templateUrl: './laptop-detail.component.html',
  styleUrls: ['./laptop-detail.component.css']
})
export class LaptopDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute , private laptopsService: LaptopsService) { }
      laptop: Laptops
  ngOnInit() {
    this.getLaptopDetail();
  }
  getLaptopDetail(){
          const id = +this.route.snapshot.paramMap.get('id');
          this.laptopsService.setLaptopDetail(id)
              .subscribe(laptop => this.laptop = laptop);
  }
}
