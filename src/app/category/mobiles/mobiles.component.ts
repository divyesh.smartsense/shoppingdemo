import { Component, OnInit } from '@angular/core';
import {Mobiles} from "./mobiles";
import {MobilesService} from "./mobiles.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-mobiles',
  templateUrl: './mobiles.component.html',
  styleUrls: ['./mobiles.component.css']
})
export class MobilesComponent implements OnInit {
    mobile : Mobiles[];
  constructor(private mobilesService: MobilesService) {
  }

  ngOnInit() {
        this.getMobiles();
  }
  getMobiles(){
     this.mobilesService.getMobiles()
         .subscribe(
             mobile => this.mobile = mobile
         );
  }

}
