import { Injectable } from '@angular/core';
import {LAPTOPS} from "./mock-laptops";
import {Observable, of} from "rxjs";
import {Laptops} from "./laptops";

@Injectable({
  providedIn: 'root'
})
export class LaptopsService {

  constructor() { }
  setLaptops(): Observable<Laptops[]>{
     return of(LAPTOPS);
  }
  setLaptopDetail(id: number): Observable<Laptops>{
    return of(LAPTOPS.find(laptop =>laptop.id === id))
  }
}
