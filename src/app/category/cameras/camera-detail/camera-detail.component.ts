import { Component, OnInit } from '@angular/core';
import {CamerasService} from "../cameras.service";
import {ActivatedRoute} from "@angular/router";
import {Cameras} from "../cameras";

@Component({
  selector: 'app-camera-detail',
  templateUrl: './camera-detail.component.html',
  styleUrls: ['./camera-detail.component.css']
})
export class CameraDetailComponent implements OnInit {

  constructor(private camerasService: CamerasService, private route: ActivatedRoute) { }
    camera: Cameras;
  ngOnInit() {
    this.getCameraDetail();
  }
  getCameraDetail(){
    const id = +this.route.snapshot.paramMap.get('id');
    this.camerasService.setCameraDetail(id)
        .subscribe(cameras => this.camera = cameras);
  }

}
