import { Injectable } from '@angular/core';
import {TABLETS} from "./mock-tablets";
import {Observable, of} from "rxjs";
import {Tablets} from "./tablets";

@Injectable({
  providedIn: 'root'
})
export class TabletsService {

  constructor() { }

  setTablets(): Observable<Tablets[]>{
    return of(TABLETS);
  }
  setTabletDetail(id: number): Observable<Tablets>{
    return of(TABLETS.find( tablet => tablet.id === id))
  }
}
