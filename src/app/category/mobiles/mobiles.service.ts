import {Injectable, OnInit} from '@angular/core';
import {MOBILES} from "./mock-mobiles";
import {Observable, of} from "rxjs";
import {Mobiles} from "./mobiles";

@Injectable({
    providedIn: 'root'
})
export class MobilesService implements OnInit{
    constructor() {
    }

    ngOnInit(){
    }

    getMobiles(): Observable<Mobiles[]>{
        return of(MOBILES);
    }
    getMobile(id: number): Observable<Mobiles> {
        return of(MOBILES.find(mobile => mobile.id === id));
    }
}
