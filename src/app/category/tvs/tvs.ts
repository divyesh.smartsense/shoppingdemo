export class Tvs {
    id:number;
    url: string;
    name: string;
    price: number;
    size: number;
}
