import { Injectable } from '@angular/core';
import {CAMERAS} from "./mock-cameras";
import {Observable, of} from "rxjs";
import {Cameras} from "./cameras";

@Injectable({
  providedIn: 'root'
})
export class CamerasService {

  constructor() { }
  setCameras(): Observable<Cameras[]>{
    return of(CAMERAS);
  }
  setCameraDetail(id: number): Observable<Cameras>{
    return of(CAMERAS.find(camera => camera.id === id));
  }

}
