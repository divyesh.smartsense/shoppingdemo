import { Injectable } from '@angular/core';
import {AngularFireDatabase} from "@angular/fire/database";
import {Product} from "./models/product";
import {take} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
  data;

  constructor(private db: AngularFireDatabase) {
  }


  private createShoppingCartData() {
    return this.db.list('/shopping-cart/').push({
      dateCreated: new Date().getTime()
    })
  }

  private async getOrCreateShoppingCartId() {
    let cartId = localStorage.getItem('cartId');
    if (cartId) return cartId;
    else {
      let result = await this.createShoppingCartData();
      localStorage.setItem('cartId', result.key);
      return result.key;
    }
  }


   getItem(cartId: string, productId: string) {
    return this.db.object('/shopping-cart/' + cartId + '/items/' + productId);
  }


  async addToCartProduct(product: Product) {
    let cartId = await this.getOrCreateShoppingCartId();
    let item$$ = this.getItem(cartId, product.title);
    let item$ = this.getItem(cartId, product.title).snapshotChanges();

    item$.pipe(take(1)).subscribe(item => {
      this.data = item.payload.val();
      if (this.data != null) {
        item$$.set({product: product, quantity: (this.data.quantity) + 1});
      } else {
        item$$.set({product: product, quantity: 1});
      }
    });
  }

  async removeToCartProduct(product: Product) {
    let cartId = await this.getOrCreateShoppingCartId();
    let item$$ = this.getItem(cartId, product.title);
    let item$ = this.getItem(cartId, product.title).snapshotChanges();

    item$.pipe(take(1)).subscribe(item => {
      this.data = item.payload.val();
      if (this.data != null) {
        item$$.set({product: product, quantity: (this.data.quantity) - 1});
      } else {
        item$$.set({product: product, quantity: 0});
      }
    });
  }

  async clearCart(){
    let cartId = await this.getOrCreateShoppingCartId();
    this.db.object('/shopping-cart/' + cartId + '/items').remove();
  }
}
