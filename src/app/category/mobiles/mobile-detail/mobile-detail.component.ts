import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {MobilesService} from "../mobiles.service";
import {Mobiles} from "../mobiles";

@Component({
  selector: 'app-mobile-detail',
  templateUrl: './mobile-detail.component.html',
  styleUrls: ['./mobile-detail.component.css']
})
export class MobileDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute, private mobilesService: MobilesService) {
  }

  mobiles: Mobiles;
  ngOnInit() {
    this.getMobileDetails();
  }

  getMobileDetails(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.mobilesService.getMobile(id)
        .subscribe(mobiles => this.mobiles = mobiles);
  }
}
