import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {ReactiveFormsModule} from "@angular/forms"
import { MobilesComponent } from './category/mobiles/mobiles.component';
import { TabletsComponent } from './category/tablets/tablets.component';
import { TvsComponent } from './category/tvs/tvs.component';
import { MobileDetailComponent } from './category/mobiles/mobile-detail/mobile-detail.component';
import {AppRoutingModule} from "./app-routing.module";
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LaptopDetailComponent } from './category/laptops/laptop-detail/laptop-detail.component';
import { CameraDetailComponent } from './category/cameras/camera-detail/camera-detail.component';
import { TabletDetailComponent } from './category/tablets/tablet-detail/tablet-detail.component';
import { TvDetailComponent } from './category/tvs/tv-detail/tv-detail.component';
import {FormsModule} from "@angular/forms";
import {CustomFormsModule} from "ng2-validation";
import { CategoryComponent } from './category/category.component';
import { LoginComponent } from './login/login.component';
import {AuthGuard} from "./auth.guard";
import {AngularFireModule} from "@angular/fire";
import {environment} from "../environments/environment";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {AngularFireAuthModule} from "@angular/fire/auth";
import {AngularFireStorageModule} from "@angular/fire/storage";
import { BsNavbarComponent } from './bs-navbar/bs-navbar.component';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { CheckOutComponent } from './check-out/check-out.component';
import { OrderSuccessComponent } from './order-success/order-success.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { AdminProductsComponent } from './admin/admin-products/admin-products.component';
import { AdminOdersComponent } from './admin/admin-oders/admin-oders.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {NgbPaginationModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from "./auth.service";
import {LaptopsComponent} from "./category/laptops/laptops.component";
import {CamerasComponent} from "./category/cameras/cameras.component";
import {UserService} from "./user.service";
import {AngularFireDatabase} from "@angular/fire/database";
import { ProductFormComponent } from './admin/product-form/product-form.component';
import {CategoryService} from "./category.service";
import { ProductFilterComponent } from './products/product-filter/product-filter.component';
import { ProductCardComponent } from './product-card/product-card.component';

@NgModule({
  declarations: [
    AppComponent,
    MobilesComponent,
    TabletsComponent,
    LaptopsComponent,
    CamerasComponent,
    TvsComponent,
    MobileDetailComponent,
    PageNotFoundComponent,
    LaptopDetailComponent,
    CameraDetailComponent,
    TabletDetailComponent,
    TvDetailComponent,
      CategoryComponent,
      LoginComponent,
      BsNavbarComponent,
      HomeComponent,
      ProductsComponent,
      ShoppingCartComponent,
      CheckOutComponent,
      OrderSuccessComponent,
      MyOrdersComponent,
      AdminProductsComponent,
      AdminOdersComponent,
      ProductFormComponent,
      ProductFilterComponent,
      ProductCardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
      FormsModule,
      CustomFormsModule,
      ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase,'shoppingdemo'),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
      NgbModule.forRoot(),
    NgbPaginationModule,
    NgbAlertModule,
  ],
  providers: [AuthGuard,AuthService,UserService,AngularFireDatabase,CategoryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
