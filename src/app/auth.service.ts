import {Injectable, OnInit} from '@angular/core';
import {AngularFireAuth } from "@angular/fire/auth";
import * as firebase from 'firebase';
import {Observable} from "rxjs";
import {ActivatedRoute} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnInit{

  user$: Observable<firebase.User>;
  constructor(private afAuth: AngularFireAuth , private route: ActivatedRoute) {
    this.user$ = afAuth.authState;
  }
  ngOnInit(): void {
  }

    logout(){
    this.afAuth.auth.signOut();
  }

  loginWithGoogle(){
    let returnUrl = this.route.snapshot.queryParamMap.get('/returnUrl') || '/';
    localStorage.setItem('returnUrl', returnUrl);
    return this.afAuth.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
  }


  checkusernameandpassword(uname: string , pwd: string){
    if(uname == 'divyesh' && pwd == 'admin123'){
      localStorage.setItem('username','pwd');
      return true;
    }
    else {
      return false;
    }
  }
}
