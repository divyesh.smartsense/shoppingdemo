import { Injectable } from '@angular/core';
import {AngularFireDatabase} from "@angular/fire/database";
import {query} from "@angular/animations";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private db: AngularFireDatabase) { }
  getCategories() {
    return this.db.list('/categories', ref => ref.orderByChild('name')).valueChanges();
  }
}
