import {Component} from '@angular/core';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {UsernameValidators} from "./username.validators";
import {AuthService} from "./auth.service";
import {Router} from "@angular/router";
import {UserService} from "./user.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  constructor(private authService: AuthService , private route: Router , private userService: UserService) {
    authService.user$.subscribe(
        user => {
          if(!user) return;

            userService.saveUser(user);

            let returnUrl = localStorage.getItem('returnUrl');
            if(!returnUrl) return;
            localStorage.removeItem('returnUrl');
            route.navigateByUrl(returnUrl);

        }
    )
  }

  form = new FormGroup({
    username: new FormControl('',[
      Validators.required,
      Validators.minLength(3),
      UsernameValidators.cannotContainSpace
    ]),
    password: new FormControl('',[
      Validators.required,
      Validators.minLength(6)
    ])
  }, );

  get username(){
    return this.form.get('username');
  }
  get password(){
    return this.form.get('password');
  }

}
