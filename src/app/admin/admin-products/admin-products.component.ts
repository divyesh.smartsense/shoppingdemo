import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProductService} from "../../product.service";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";
import {Product} from "../../models/product";
@Component({
  selector: 'app-admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.css']
})
export class AdminProductsComponent implements OnInit , OnDestroy {
    product:any[];
    filteredProduct: any[];
    subscription: Subscription;
    id;

  constructor(private productService: ProductService, private router: Router) {
    this.subscription = this.productService.getAllProduct().snapshotChanges()
        .subscribe(product => this.filteredProduct = this.product = product);

  }
  deleteProduct() {
    if (!confirm('Are You Want To Delete Product?')) return;
    else {
      this.productService.deleteProduct(this.id);
      this.router.navigate(['/admin/products']);
    }
  }

  filter(query: string){
    this.filteredProduct = (query)?
        this.product.filter(p => p.title.toLowerCase().includes(query.toLowerCase())):
    this.product;
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
