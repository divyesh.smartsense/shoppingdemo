import {Tablets} from "./tablets";

export const TABLETS: Tablets[] = [
    {
        id:1,
        url: "https://www.91-img.com/pictures/apple-ipad-pro-9.7-wifi-cellular-32gb-tablet-medium-1.jpg",
        name: "Apple iPad Pro 9.7 WiFi Cellular 32GB",
        price: 38000,
        performance: "Dualcore",
        display: 9.7,
        camera: "12MP",
        battery: 7306
    },
    {
        id:2,
        url: "https://www.91-img.com/pictures/72794-v1-samsung-galaxy-tab-a-9.7-tablet-medium-1.jpg",
        name: "Samsung Galaxy Tab A 9.79",
        price: 41000,
        performance: "Quadcore",
        display: 9.7,
        camera: "8.1",
        battery: 6000
    },
    {
        id:3,
        url: "https://www.91-img.com/pictures/iball-slide-brace-xj-tablet-medium-1.jpg",
        name: "iBall Slide Brace XJ",
        price: 16999,
        performance: "Octacore",
        display: 10.1,
        camera: "8",
        battery: 7800
    },
    {
        id:4,
        url: "https://www.91-img.com/pictures/skytel-edumedia-tab-801-tablet-medium-1.jpg",
        name: "Skytel Edumedia Tab 801",
        price: 18000,
        performance: "Dualcore",
        display: 8,
        camera: "4.1",
        battery: 4000
    }

];
