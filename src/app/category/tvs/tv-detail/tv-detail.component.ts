import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {TvsService} from "../tvs.service";
import {Tvs} from "../tvs";

@Component({
  selector: 'app-tv-detail',
  templateUrl: './tv-detail.component.html',
  styleUrls: ['./tv-detail.component.css']
})
export class TvDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute , private tvsService: TvsService) { }
      tv: Tvs;
  ngOnInit() {
    this.getTvDetail();
  }
  getTvDetail(){
      const id = +this.route.snapshot.paramMap.get('id');
      this.tvsService.setTvsDetail(id)
          .subscribe( tv => this.tv = tv);
  }
}
