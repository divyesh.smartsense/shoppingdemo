import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UsernameValidators} from "../username.validators";
import {AuthService} from "../auth.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

    user$: any;
    // Form variables
    loginForm: FormGroup;

    msg = '';

    constructor(private route: Router,
                private _fb: FormBuilder,
                public authService: AuthService) {
    }

    get username() {
        return this.loginForm.get('username');
    }

    get password() {
        return this.loginForm.get('password');
    }

    ngOnInit() {
        this.createLoginForm();

        this.user$ = this.authService.user$.subscribe(response => {
            if(response) {
                window.localStorage.setItem('user-name', response.displayName);
            }
            })
    }


    createLoginForm = () => {
        this.loginForm = this._fb.group({
            username: ['', [
                Validators.required,
                Validators.minLength(3),
                UsernameValidators.cannotContainSpace
            ]],
            password: ['', [
                Validators.required,
                Validators.minLength(6)
            ]]
        },);
    };

    loginWithGoogle() {
        this.authService.loginWithGoogle();
    }

    onLoginClick(uname: string, pwd: string) {
        var output = this.authService.checkusernameandpassword(uname, pwd);
        if (output) {
            if (uname == 'divyesh' && pwd == 'admin123') {
                this.route.navigate(['/Login/Category']);
            }
        } else {
            this.msg = 'Username & Password are Invalid';
        }
    }

    ngOnDestroy(): void {
        if (this.user$) {
            this.user$.unsubscribe();
        }
    }

}
