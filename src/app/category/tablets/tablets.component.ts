import { Component, OnInit } from '@angular/core';
import {TabletsService} from "./tablets.service";
import {Tablets} from "./tablets";

@Component({
  selector: 'app-tablets',
  templateUrl: './tablets.component.html',
  styleUrls: ['./tablets.component.css']
})
export class TabletsComponent implements OnInit {
    tablet: Tablets[] =[];
  constructor(private tabletsService: TabletsService) { }

  ngOnInit() {
    this.getTablets();
  }
  getTablets(){
    this.tabletsService.setTablets()
        .subscribe(tablet => this.tablet = tablet);
  }

}
