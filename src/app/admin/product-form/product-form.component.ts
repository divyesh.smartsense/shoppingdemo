import {Component, OnInit} from '@angular/core';
import {CategoryService} from "../../category.service";
import {Observable} from "rxjs";
import {ProductService} from "../../product.service";
import {ActivatedRoute, Router} from "@angular/router";
import {take} from "rxjs/operators";

@Component({
    selector: 'app-product-form',
    templateUrl: './product-form.component.html',
    styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {
    categories$: Observable<any[]>;
    product = {};
    id;

    constructor(private categoryService: CategoryService,
                private productService: ProductService,
                private router: Router,
                private route: ActivatedRoute) {
    }


    saveProduct(product) {
        if (this.id) {
            this.productService.updateProduct(product, this.id);
        } else {
            this.productService.createProduct(product);
        }

        this.router.navigate(['/admin/products']);
    }
    deleteProduct(){
            if(!confirm('Are You Want To Delete Product?')) return;
            else {
                this.productService.deleteProduct(this.id);
                this.router.navigate(['/admin/products']);
            }
    }


    ngOnInit() {
        this.categories$ = this.categoryService.getCategories();
        this.routeSubscriber();

    }

    routeSubscriber = ()=>{
        this.id = this.route.snapshot.paramMap.get('id');
        if (this.id) {
            this.productService.getProductId(this.id).valueChanges().pipe(take(1)).subscribe(p => this.product = p);
        }
    }
}
