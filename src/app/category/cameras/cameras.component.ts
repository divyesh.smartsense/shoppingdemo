import { Component, OnInit } from '@angular/core';
import {Cameras} from "./cameras";
import {CamerasService} from "./cameras.service";

@Component({
  selector: 'app-cameras',
  templateUrl: './cameras.component.html',
  styleUrls: ['./cameras.component.css']
})
export class CamerasComponent implements OnInit {
    cameras: Cameras[] =[];
  constructor(private camerasService: CamerasService) { }

  ngOnInit() {
    this.getCamera();
  }
  getCamera(){
    this.camerasService.setCameras()
        .subscribe(cameras => this.cameras = cameras);
  }

}
